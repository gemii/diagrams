/**
 * Created by walter on 7/20/16.
 */
/*数据同步加载*/
var width = 900;
var height = 900;
function dataLoad(filename)  {
    var arraydata;
    $.ajax({
        type: "GET",
        url: filename,
        dataType: "json",
        async: false,
        success: function(json){arraydata = eval(json) }
    });
    return arraydata;
}
var dataFull = dataLoad("jsonFile/report_daily_20160701.json");
var data = dataFull.data;
console.log(data);
console.log(d3.max(data[1],function(d){
    return d.MonitorMentioned;
}));


/*svg分类*/
var svg_histo_TotalMsgCount = d3.selectAll(".histogram_TotalMsgCount")
    .append("svg")
    .attr("width",width)
    .attr("height",height)

var svg_histo_ActiveUserCount = d3.selectAll(".histogram_ActiveUserCount")
    .append("svg")
    .attr("width",width)
    .attr("height",height)

var svg_histo_MonitorChatCount = d3.selectAll(".histogram_MonitorChatCount")
    .append("svg")
    .attr("width",width)
    .attr("height",height)

var svg_histo_MonitorMentioned = d3.selectAll(".histogram_MonitorMentioned")
    .append("svg")
    .attr("width",width)
    .attr("height",height)
/*数据封装*/
var maindiagram = function(i,n,e){
    var utilData = data[i];
    console.log("real" + utilData);
//    var valueMax = d3.max(utilData,function(d){
//       return d.TotalMsgCount;
//    })
    switch (e) {

        case "TotalMsgCount":
        /* 内容*/
        var valueMax = 4000;
            console.log("max" + valueMax);
            var number = n;
            var realData = new Array();
            console.log("chengji" + (valueMax / number));
            for (j = 0; j < number; j++) {
                var count = 0;
                for (i = 0; i < utilData.length; i++) {
//            console.log("iiisd"+j*(valueMax/number))
                    if (j * (valueMax / number) < utilData[i].TotalMsgCount && utilData[i].TotalMsgCount <= (j + 1) * (valueMax / number)) {
                        count++;
//                console.log("count"+count)
                        console.log(utilData[i].TotalMsgCount)
                    }
                }
                var arr = {"x": j * (valueMax / number), "y": count}
                realData.push(arr);
                console.log("CountA" + count);
            }
            console.log("CountB" + count);
            console.log("AAA" + d3.max(utilData, function (d) {
                    return d.diagramName;
                }));
            console.log("BBB" + d3.max(realData, function (d) {
                    return d.y
                }))


            /*TotalMsgCount绘图操作*/
            /*画轴*/
            var xScale = d3.scale.linear()
                .domain([0, valueMax])
                .range([0, 500])
            var yScale = d3.scale.linear()
                .domain([100 + d3.max(realData, function (d) {
                    return d.y;
                }), 0])
                .range([0, 700]);
            var xAxis_histo_TotalMsgCount = d3.svg.axis()
                .scale(xScale)
                .orient("bottom")
                .ticks(number)
            var yAxis_histo_TotalMsgCount = d3.svg.axis()
                .scale(yScale)
                .orient("left")
            var histo_gxAxis = svg_histo_TotalMsgCount.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,720)")
                .call(xAxis_histo_TotalMsgCount)
                .append("text")
                .text(e)
                .attr("transform", "translate(520,2)")
            var histo_gyAxis = svg_histo_TotalMsgCount.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,20)")
                .call(yAxis_histo_TotalMsgCount)
                .append("text")
                .text("Frequency")
                .attr("transform", "translate(10,-5)")
            color = d3.scale.category20c();
            /*画线*/
            svg_histo_TotalMsgCount.selectAll(".bar")
                .data(realData)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) {
                    return xScale(d.x)+1
                })
                .attr("width", xScale((valueMax / number))-2)
                .attr("y", function (d) {
                    return yScale(d.y)
                })
                .attr("height", function (d) {
                    return height - yScale(d.y) - 200
                })
                .attr("fill", function (d) {
                    return "rgb(0,0," + Math.round(d.y / 10 + 100) + ")";
                })
                .attr("transform", "translate(40,20)")
            break;

        case "ActiveUserCount":
            var valueMax = 200;
            console.log("max" + valueMax);
            var number = n;
            var realData = new Array();
            console.log("chengji" + (valueMax / number));
            for (j = 0; j < number; j++) {
                var count = 0;
                for (i = 0; i < utilData.length; i++) {
//            console.log("iiisd"+j*(valueMax/number))
                    if (j * (valueMax / number) < utilData[i].ActiveUserCount && utilData[i].ActiveUserCount <= (j + 1) * (valueMax / number)) {
                        count++;
//                console.log("count"+count)
                        console.log(utilData[i].ActiveUserCount)
                    }
                }
                var arr = {"x": j * (valueMax / number), "y": count}
                realData.push(arr);
                console.log("CountA" + count);
            }
            console.log("CountB" + count);
            console.log("AAA" + d3.max(utilData, function (d) {
                    return d.ActiveUserCount;
                }));
            console.log("BBB" + d3.max(realData, function (d) {
                    return d.y
                }))
            /*ctiveUserCount绘图操作*/
            /*画轴*/
            var xScale = d3.scale.linear()
                .domain([0, valueMax])
                .range([0, 500])
            var yScale = d3.scale.linear()
                .domain([50 + d3.max(realData, function (d) {
                    return d.y;
                }), 0])
                .range([0, 700]);
            var xAxis_histo_ActiveUserCount = d3.svg.axis()
                .scale(xScale)
                .orient("bottom")
                .ticks(number)
            var yAxis_histo_ActiveUserCount = d3.svg.axis()
                .scale(yScale)
                .orient("left")
            var histo_gxAxis = svg_histo_ActiveUserCount.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,720)")
                .call(xAxis_histo_ActiveUserCount)
                .append("text")
                .text(e)
                .attr("transform", "translate(520,2)")
            var histo_gyAxis = svg_histo_ActiveUserCount.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,20)")
                .call(yAxis_histo_ActiveUserCount)
                .append("text")
                .text("Frequency")
                .attr("transform", "translate(10,-5)")
            color = d3.scale.category20c();
            /*画线*/
            svg_histo_ActiveUserCount.selectAll(".bar")
                .data(realData)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) {
                    return xScale(d.x)+1
                })
                .attr("width", xScale((valueMax / number))-2)
                .attr("y", function (d) {
                    return yScale(d.y)
                })
                .attr("height", function (d) {
                    return height - yScale(d.y) - 200
                })
                .attr("fill", function (d) {
                    return "rgb(0,0," + Math.round(d.y / 10 + 100) + ")";
                })
                .attr("transform", "translate(40,20)")
            break;

        case "MonitorChatCount":
            var valueMax = 200;
            console.log("max" + valueMax);
            var number = n;
            var realData = new Array();
            console.log("chengji" + (valueMax / number));
            for (j = 0; j < number; j++) {
                var count = 0;
                for (i = 0; i < utilData.length; i++) {
//            console.log("iiisd"+j*(valueMax/number))
                    if (j * (valueMax / number) < utilData[i].MonitorChatCount && utilData[i].MonitorChatCount <= (j + 1) * (valueMax / number)) {
                        count++;
//                console.log("count"+count)
                        console.log(utilData[i].MonitorChatCount)
                    }
                }
                var arr = {"x": j * (valueMax / number), "y": count}
                realData.push(arr);
                console.log("CountA" + count);
            }
            console.log("CountB" + count);
            console.log("AAA" + d3.max(utilData, function (d) {
                    return d.MonitorChatCount;
                }));
            console.log("BBB" + d3.max(realData, function (d) {
                    return d.y
                }))


            /*MonitorChatCount绘图操作*/
            /*画轴*/
            var xScale = d3.scale.linear()
                .domain([0, valueMax])
                .range([0, 500])
            var yScale = d3.scale.linear()
                .domain([50 + d3.max(realData, function (d) {
                    return d.y;
                }), 0])
                .range([0, 700]);
            var xAxis_histo_MonitorChatCount = d3.svg.axis()
                .scale(xScale)
                .orient("bottom")
                .ticks(number)
            var yAxis_histo_MonitorChatCount = d3.svg.axis()
                .scale(yScale)
                .orient("left")
            var histo_gxAxis = svg_histo_MonitorChatCount.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,720)")
                .call(xAxis_histo_MonitorChatCount)
                .append("text")
                .text(e)
                .attr("transform", "translate(520,2)")
            var histo_gyAxis = svg_histo_MonitorChatCount.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,20)")
                .call(yAxis_histo_MonitorChatCount)
                .append("text")
                .text("Frequency")
                .attr("transform", "translate(10,-5)")
            color = d3.scale.category20c();
            /*画线*/
            svg_histo_MonitorChatCount.selectAll(".bar")
                .data(realData)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) {
                    return xScale(d.x)+1
                })
                .attr("width", xScale((valueMax / number))-2)
                .attr("y", function (d) {
                    return yScale(d.y)
                })
                .attr("height", function (d) {
                    return height - yScale(d.y) - 200
                })
                .attr("fill", function (d) {
                    return "rgb(0,0," + Math.round(d.y / 10 + 100) + ")";
                })
                .attr("transform", "translate(40,20)")
            break;

        case "MonitorMentioned":
            var valueMax = 200;
            console.log("max" + valueMax);
            var number = n;
            var realData = new Array();
            console.log("chengji" + (valueMax / number));
            for (j = 0; j < number; j++) {
                var count = 0;
                for (i = 0; i < utilData.length; i++) {
//            console.log("iiisd"+j*(valueMax/number))
                    if (j * (valueMax / number) < utilData[i].MonitorMentioned && utilData[i].MonitorMentioned <= (j + 1) * (valueMax / number)) {
                        count++;
//                console.log("count"+count)
                        console.log(utilData[i].MonitorMentioned)
                    }
                }
                var arr = {"x": j * (valueMax / number), "y": count}
                realData.push(arr);
                console.log("CountA" + count);
            }
            console.log("CountB" + count);
            console.log("AAA" + d3.max(utilData, function (d) {
                    return d.MonitorMentioned;
                }));
            console.log("BBB" + d3.max(realData, function (d) {
                    return d.y
                }))


            /*MonitorMentioned绘图操作*/
            /*画轴*/
            var xScale = d3.scale.linear()
                .domain([0, valueMax])
                .range([0, 500])
            var yScale = d3.scale.linear()
                .domain([50 + d3.max(realData, function (d) {
                    return d.y;
                }), 0])
                .range([0, 700]);
            var xAxis_histo_MonitorMentioned = d3.svg.axis()
                .scale(xScale)
                .orient("bottom")
                .ticks(number)
            var yAxis_histo_MonitorMentioned = d3.svg.axis()
                .scale(yScale)
                .orient("left")
            var histo_gxAxis = svg_histo_MonitorMentioned.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,720)")
                .call(xAxis_histo_MonitorMentioned)
                .append("text")
                .text(e)
                .attr("transform", "translate(520,2)")
            var histo_gyAxis = svg_histo_MonitorMentioned.append("g")
                .attr("class", "axis_histo")
                .attr("transform", "translate(40,20)")
                .call(yAxis_histo_MonitorMentioned)
                .append("text")
                .text("Frequency")
                .attr("transform", "translate(10,-5)")
            color = d3.scale.category20c();
            /*画线*/
            svg_histo_MonitorMentioned.selectAll(".bar")
                .data(realData)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) {
                    return xScale(d.x)+1
                })
                .attr("width", xScale((valueMax / number))-2)
                .attr("y", function (d) {
                    return yScale(d.y)
                })
                .attr("height", function (d) {
                    return height - yScale(d.y) - 200
                })
                .attr("fill", function (d) {
                    return "rgb(0,0," + Math.round(d.y / 10 + 100) + ")";
                })
                .attr("transform", "translate(40,20)")
            break;
    }

}

var saveValue_histo = {"x": 1, "y": 1}
var rankbox_TotalMsgCount = svg_histo_TotalMsgCount.append("rect")
    .attr("class","divBox_histo")
    .attr("x", 30)
    .attr("y", 800)
    .attr("width", 420)
    .attr("height", 40)
    .attr("fill","black")
    .style("opacity",0.1)
    .on("mouseover", function (){
        rankbox_TotalMsgCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousedown", mousemove)
            .on("touchmove", mousemove)
        function mousemove() {
            d3.selectAll(".lable_histo")
                .transition()
                .duration(80)
                .attr("cx", Math.round(d3.mouse(this)[0]));
            console.log("text--" + Math.round(divideRank_TotalMsgCount.invert(d3.mouse(this)[0])),"TotalMsgCount");
            console.log("mouse" + d3.mouse(this)[0])
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.x = Math.round(divideRank_TotalMsgCount.invert(d3.mouse(this)[0]))
            maindiagram(saveValue_histo.y, Math.round(divideRank_TotalMsgCount.invert(d3.mouse(this)[0])),"TotalMsgCount")
        }
        function mouseout() {
            circle_label_TotalMsgCount.classed("active", false);
        }
        function mouseover() {
            circle_label_TotalMsgCount.classed("active", true);
        }
    });




var circle_label_TotalMsgCount = svg_histo_TotalMsgCount.append("circle")
    .attr("class", "lable_histo")
    .attr("text-anchor", "end")
    .attr("cx", 40)
    .attr("cy", 810)
    .attr("r",8)
    .attr("fill","orange")
    .style("opacity",1)
// var circle_TotalMsgCount = circle_label_TotalMsgCount.node().getBBox();
var divideRank_TotalMsgCount = d3.scale.linear()
    .domain([0, 6])
    .range([0, 400])
    .clamp(true);
var rankAxis_TotalMsgCount = d3.svg.axis()
    .scale(divideRank_TotalMsgCount)
    .orient("bottom")
    .ticks(7)
var gtimeAxis = svg_histo_TotalMsgCount.append("g")
    .attr("class","axis_line")
    .attr("transform","translate(40,820)")
    .call(rankAxis_TotalMsgCount)




/**/

var label_histo_TotalMsgCount = svg_histo_TotalMsgCount.append("text")
    .attr("class", "label_time")
    .attr("text-anchor", "end")
    .attr("x", 600)
    .attr("y", 860)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_histo_TotalMsgCount = label_histo_TotalMsgCount.node().getBBox();
var yearScale_histo_TotalMsgCount = d3.scale.linear()
    .domain([1, 7])
    .range([box_histo_TotalMsgCount.x + 10, box_histo_TotalMsgCount.x + box_histo_TotalMsgCount.width - 10])
    .clamp(true);
var yearbox_histo_TotalMsgCount = svg_histo_TotalMsgCount.append("rect")
    .attr("class","yearbox_time")
    .attr("x", box_histo_TotalMsgCount.x)
    .attr("y", box_histo_TotalMsgCount.y)
    .attr("width", box_histo_TotalMsgCount.width)
    .attr("height", box_histo_TotalMsgCount.height)
    .on("mouseover", function () {
        yearbox_histo_TotalMsgCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_histo_TotalMsgCount.classed("active", true);
        }
        function mouseout() {
            label_histo_TotalMsgCount.classed("active", false);
        }
        function mousemove() {
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.y = (Math.round(yearScale_histo_TotalMsgCount.invert(d3.mouse(this)[0]))-1);
            maindiagram(Math.round(yearScale_histo_TotalMsgCount.invert(d3.mouse(this)[0]))-1,saveValue_histo.x,"TotalMsgCount")
            label_histo_TotalMsgCount.text(Math.round(yearScale_histo_TotalMsgCount.invert(d3.mouse(this)[0])));
        }
    });


/*ActiveUserCount操作台*/
var saveValue_histo = {"x": 1, "y": 1}
var rankbox_ActiveUserCount = svg_histo_ActiveUserCount.append("rect")
    .attr("class","divBox_histo")
    .attr("x", 30)
    .attr("y", 800)
    .attr("width", 420)
    .attr("height", 40)
    .attr("fill","black")
    .style("opacity",0.1)
    .on("mouseover", function (){
        rankbox_ActiveUserCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousedown", mousemove)
            .on("touchmove", mousemove)
        function mousemove() {
            d3.selectAll(".lable_histo")
                .transition()
                .duration(80)
                .attr("cx", Math.round(d3.mouse(this)[0]));
            console.log("text--" + Math.round(divideRank_ActiveUserCount.invert(d3.mouse(this)[0])),"ActiveUserCount");
            console.log("mouse" + d3.mouse(this)[0])
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.x = Math.round(divideRank_ActiveUserCount.invert(d3.mouse(this)[0]))
            maindiagram(saveValue_histo.y, Math.round(divideRank_ActiveUserCount.invert(d3.mouse(this)[0])),"ActiveUserCount")
        }
        function mouseout() {
            circle_label_ActiveUserCount.classed("active", false);
        }
        function mouseover() {
            circle_label_ActiveUserCount.classed("active", true);
        }
    });




var circle_label_ActiveUserCount = svg_histo_ActiveUserCount.append("circle")
    .attr("class", "lable_histo")
    .attr("text-anchor", "end")
    .attr("cx", 40)
    .attr("cy", 810)
    .attr("r",8)
    .attr("fill","orange")
    .style("opacity",1)
// var circle_TotalMsgCount = circle_label_TotalMsgCount.node().getBBox();
var divideRank_ActiveUserCount = d3.scale.linear()
    .domain([0, 6])
    .range([0, 400])
    .clamp(true);
var rankAxis_ActiveUserCount= d3.svg.axis()
    .scale(divideRank_TotalMsgCount)
    .orient("bottom")
    .ticks(7)
var gtimeAxis = svg_histo_ActiveUserCount.append("g")
    .attr("class","axis_line")
    .attr("transform","translate(40,820)")
    .call(rankAxis_ActiveUserCount)




/**/

var label_histo_ActiveUserCount = svg_histo_ActiveUserCount.append("text")
    .attr("class", "label_time")
    .attr("text-anchor", "end")
    .attr("x", 600)
    .attr("y", 860)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_histo_ActiveUserCount = label_histo_ActiveUserCount.node().getBBox();
var yearScale_histo_ActiveUserCount = d3.scale.linear()
    .domain([1, 7])
    .range([box_histo_ActiveUserCount.x + 10, box_histo_ActiveUserCount.x + box_histo_ActiveUserCount.width - 10])
    .clamp(true);
var yearbox_histo_ActiveUserCount = svg_histo_ActiveUserCount.append("rect")
    .attr("class","yearbox_time")
    .attr("x", box_histo_ActiveUserCount.x)
    .attr("y", box_histo_ActiveUserCount.y)
    .attr("width", box_histo_ActiveUserCount.width)
    .attr("height", box_histo_ActiveUserCount.height)
    .on("mouseover", function () {
        yearbox_histo_ActiveUserCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_histo_ActiveUserCount.classed("active", true);
        }
        function mouseout() {
            label_histo_ActiveUserCount.classed("active", false);
        }
        function mousemove() {
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.y = (Math.round(yearScale_histo_ActiveUserCount.invert(d3.mouse(this)[0]))-1);
            maindiagram(Math.round(yearScale_histo_ActiveUserCount.invert(d3.mouse(this)[0]))-1,saveValue_histo.x,"ActiveUserCount")
            label_histo_ActiveUserCount.text(Math.round(yearScale_histo_ActiveUserCount.invert(d3.mouse(this)[0])));
        }
    });




/*MonitorChatCount操作台*/


var saveValue_histo = {"x": 1, "y": 1}
var rankbox_MonitorChatCount = svg_histo_MonitorChatCount.append("rect")
    .attr("class","divBox_histo")
    .attr("x", 30)
    .attr("y", 800)
    .attr("width", 420)
    .attr("height", 40)
    .attr("fill","black")
    .style("opacity",0.1)
    .on("mouseover", function (){
        rankbox_MonitorChatCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousedown", mousemove)
            .on("touchmove", mousemove)
        function mousemove() {
            d3.selectAll(".lable_histo")
                .transition()
                .duration(80)
                .attr("cx", Math.round(d3.mouse(this)[0]));
            console.log("text--" + Math.round(divideRank_MonitorChatCount.invert(d3.mouse(this)[0])),"MonitorChatCount");
            console.log("mouse" + d3.mouse(this)[0])
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.x = Math.round(divideRank_MonitorChatCount.invert(d3.mouse(this)[0]))
            maindiagram(saveValue_histo.y, Math.round(divideRank_MonitorChatCount.invert(d3.mouse(this)[0])),"MonitorChatCount")
        }
        function mouseout() {
            circle_label_MonitorChatCount.classed("active", false);
        }
        function mouseover() {
            circle_label_MonitorChatCount.classed("active", true);
        }
    });




var circle_label_MonitorChatCount = svg_histo_MonitorChatCount.append("circle")
    .attr("class", "lable_histo")
    .attr("text-anchor", "end")
    .attr("cx", 40)
    .attr("cy", 810)
    .attr("r",8)
    .attr("fill","orange")
    .style("opacity",1)
// var circle_TotalMsgCount = circle_label_TotalMsgCount.node().getBBox();
var divideRank_MonitorChatCount = d3.scale.linear()
    .domain([0, 6])
    .range([0, 400])
    .clamp(true);
var rankAxis_MonitorChatCount= d3.svg.axis()
    .scale(divideRank_MonitorChatCount)
    .orient("bottom")
    .ticks(7)
var gtimeAxis = svg_histo_MonitorChatCount.append("g")
    .attr("class","axis_line")
    .attr("transform","translate(40,820)")
    .call(rankAxis_MonitorChatCount)




/**/

var label_histo_MonitorChatCount = svg_histo_MonitorChatCount.append("text")
    .attr("class", "label_time")
    .attr("text-anchor", "end")
    .attr("x", 600)
    .attr("y", 860)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_histo_MonitorChatCount = label_histo_MonitorChatCount.node().getBBox();
var yearScale_histo_MonitorChatCount = d3.scale.linear()
    .domain([1, 7])
    .range([box_histo_MonitorChatCount.x + 10, box_histo_MonitorChatCount.x + box_histo_MonitorChatCount.width - 10])
    .clamp(true);
var yearbox_histo_MonitorChatCount = svg_histo_MonitorChatCount.append("rect")
    .attr("class","yearbox_time")
    .attr("x", box_histo_MonitorChatCount.x)
    .attr("y", box_histo_MonitorChatCount.y)
    .attr("width", box_histo_MonitorChatCount.width)
    .attr("height", box_histo_MonitorChatCount.height)
    .on("mouseover", function () {
        yearbox_histo_MonitorChatCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_histo_MonitorChatCount.classed("active", true);
        }
        function mouseout() {
            label_histo_MonitorChatCount.classed("active", false);
        }
        function mousemove() {
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.y = (Math.round(yearScale_histo_MonitorChatCount.invert(d3.mouse(this)[0]))-1);
            maindiagram(Math.round(yearScale_histo_MonitorChatCount.invert(d3.mouse(this)[0]))-1,saveValue_histo.x,"MonitorChatCount")
            label_histo_MonitorChatCount.text(Math.round(yearScale_histo_MonitorChatCount.invert(d3.mouse(this)[0])));
        }
    });




/*MonitorMentioned操作台*/


var saveValue_histo = {"x": 1, "y": 1}
var rankbox_MonitorMentioned = svg_histo_MonitorMentioned.append("rect")
    .attr("class","divBox_histo")
    .attr("x", 30)
    .attr("y", 800)
    .attr("width", 420)
    .attr("height", 40)
    .attr("fill","black")
    .style("opacity",0.1)
    .on("mouseover", function (){
        rankbox_MonitorMentioned
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousedown", mousemove)
            .on("touchmove", mousemove)
        function mousemove() {
            d3.selectAll(".lable_histo")
                .transition()
                .duration(80)
                .attr("cx", Math.round(d3.mouse(this)[0]));
            console.log("text--" + Math.round(divideRank_MonitorMentioned.invert(d3.mouse(this)[0])),"MonitorMentioned");
            console.log("mouse" + d3.mouse(this)[0])
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.x = Math.round(divideRank_MonitorMentioned.invert(d3.mouse(this)[0]))
            maindiagram(saveValue_histo.y, Math.round(divideRank_MonitorMentioned.invert(d3.mouse(this)[0])),"MonitorMentioned")
        }
        function mouseout() {
            circle_label_MonitorMentioned.classed("active", false);
        }
        function mouseover() {
            circle_label_MonitorMentioned.classed("active", true);
        }
    });




var circle_label_MonitorMentioned = svg_histo_MonitorMentioned.append("circle")
    .attr("class", "lable_histo")
    .attr("text-anchor", "end")
    .attr("cx", 40)
    .attr("cy", 810)
    .attr("r",8)
    .attr("fill","orange")
    .style("opacity",1)
// var circle_TotalMsgCount = circle_label_TotalMsgCount.node().getBBox();
var divideRank_MonitorMentioned = d3.scale.linear()
    .domain([0, 6])
    .range([0, 400])
    .clamp(true);
var rankAxis_MonitorMentioned= d3.svg.axis()
    .scale(divideRank_MonitorMentioned)
    .orient("bottom")
    .ticks(7)
var gtimeAxis = svg_histo_MonitorMentioned.append("g")
    .attr("class","axis_line")
    .attr("transform","translate(40,820)")
    .call(rankAxis_MonitorMentioned)




/**/

var label_histo_MonitorMentioned = svg_histo_MonitorMentioned.append("text")
    .attr("class", "label_time")
    .attr("text-anchor", "end")
    .attr("x", 600)
    .attr("y", 860)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_histo_MonitorMentioned = label_histo_MonitorMentioned.node().getBBox();
var yearScale_histo_MonitorMentioned = d3.scale.linear()
    .domain([1, 7])
    .range([box_histo_MonitorMentioned.x + 10, box_histo_MonitorMentioned.x + box_histo_MonitorMentioned.width - 10])
    .clamp(true);
var yearbox_histo_MonitorMentioned = svg_histo_MonitorMentioned.append("rect")
    .attr("class","yearbox_time")
    .attr("x", box_histo_MonitorMentioned.x)
    .attr("y", box_histo_MonitorMentioned.y)
    .attr("width", box_histo_MonitorMentioned.width)
    .attr("height", box_histo_MonitorMentioned.height)
    .on("mouseover", function () {
        yearbox_histo_MonitorMentioned
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_histo_MonitorMentioned.classed("active", true);
        }
        function mouseout() {
            label_histo_MonitorMentioned.classed("active", false);
        }
        function mousemove() {
            d3.selectAll(".axis_histo").remove();
            d3.selectAll(".bar").remove();
            saveValue_histo.y = (Math.round(yearScale_histo_MonitorMentioned.invert(d3.mouse(this)[0]))-1);
            maindiagram(Math.round(yearScale_histo_MonitorMentioned.invert(d3.mouse(this)[0]))-1,saveValue_histo.x,"MonitorMentioned")
            label_histo_MonitorMentioned.text(Math.round(yearScale_histo_MonitorMentioned.invert(d3.mouse(this)[0])));
        }
    });

