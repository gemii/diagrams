/**
 * Created by walter on 7/18/16.
 */
var width = 1000;
var height = 700;

/*数据同步加载*/
function dataLoad(filename)  {
    var arraydata;
    $.ajax({
        type: "GET",
        url: filename,
        dataType: "json",
        async: false,
        success: function(json) {arraydata = eval(json) }
    });
    return arraydata;
}
var dataFUll = dataLoad("jsonFile/report_daily_20160701.json");
var data = dataFUll.data;
console.log(data);
console.log(d3.max(data[1],function (d) {
    return d.MonitorMentioned;
}));

//    data.forEach(function (d) {
//        d.time = new Date(d.time);
//    })
/*数据更具时间排序分流*/

var changeDate = function (i,className) {
//        console.log("rank"+Math.round(rank))
    var newData = data[(i-1)]
    updateDate(newData,className);
}

/*数据更具函数群号分流*/




/*绘图操作*/

//    var xMax = d3.max(time0,function (d) {
//        return d.x;
//    })
//    var xMin = d3.min(time0,function (d) {
//        return d.x;
//    })
//    var yMax = d3.max(time0,function (d) {
//        return d.y;
//    })
//    var yMin = d3.min(time0,function (d) {
//        return d.y;
//    })
var bubble_TotalMsgCount= d3.selectAll(".bubble_TotalMsgCount")
var svg_TotalMsgCount = bubble_TotalMsgCount.append("svg")
    .attr("width",width)
    .attr("height",height)
var playground = svg_TotalMsgCount.append("rect")
    .attr("x", 40)
    .attr("y", 20)
    .attr("width", width-400)
    .attr("height", height-200)
    .style("fill","rgb(245,248,253)")

var color = d3.scale.category20c();                 //设置不同颜色
console.log("color"+color(1));

var xScale_TotalMsgCount = d3.scale.linear()
    .domain([0,100])
    .range([0,600])
var yScale_TotalMsgCount = d3.scale.linear()
    .domain([5000,0])
    .range([0,500]);

var xAxis_TotalMsgCount = d3.svg.axis()
    .scale(xScale_TotalMsgCount)
    .orient("bottom")
// .ticks(6)
var yAxis_TotalMsgCount = d3.svg.axis()
    .scale(yScale_TotalMsgCount)
    .orient("left")

var xline_TotalMsgCount = d3.svg.axis()
    .scale(xScale_TotalMsgCount)
    .tickSize(500,0,0)
    .orient("bottom")
var yline_TotalMsgCount = d3.svg.axis()
    .scale(yScale_TotalMsgCount)
    .tickSize(900,0,0)
    .orient("left")
var xgrid = svg_TotalMsgCount.append("g")
    .attr("class","grid")
    .attr("transform","translate(40,20)")
    .call(xline_TotalMsgCount)
    .selectAll("text")
    .text("")
var ygrid = svg_TotalMsgCount.append("g")
    .attr("class","grid")
    .attr("transform","translate(640,20)")
    .call(yline_TotalMsgCount)
    .selectAll("text")
    .text("")
var gxAxis = svg_TotalMsgCount.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,520)")
    .call(xAxis_TotalMsgCount)
    .append("text")
    .text("ActiveDegree/%")
    .attr("transform","translate(620,2)")

var gyAxis = svg_TotalMsgCount.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,20)")
    .call(yAxis_TotalMsgCount)
    .append("text")
    .text("TotalMsgCount")
    .attr("transform","translate(10,-5)")
var tipText_TotalMsgCount = d3.selectAll(".bubble_TotalMsgCount .tip-text")
svg_TotalMsgCount.append("g")
    .attr("transform","translate(40,20)")
    .selectAll(".bubble_TotalMsgCount .dot")
    .data(data[0])
    .enter()
    .append("circle")
    .attr("class","dot")
    .attr("cx", function(d) { return xScale_TotalMsgCount(d.ActiveDegree);})
    .attr("cy", function(d) { return yScale_TotalMsgCount(d.TotalMsgCount);})
    .attr("r", function (d) { return (d.UserCount/100);
    })
    .style("fill",function (d) {
        return color(1)
    })
    .attr("transform","translate(0,0)")
    .on("mouseover",function (d) {
        tip_TotalMsgCount.attr("fill","red")
            .attr("x",((d3.event.pageX))+"px")
            .attr("y",(d3.event.pageY+10)+"px")
            .style("display","block")
            .style("opacity",0.1)
            .style("background","red")
            .style("box-sizing","border-box")
            .style("border-radius",2+"px")

        tipText_TotalMsgCount
            .style("left",((d3.event.pageX)+10)+"px")
            .style("top",(d3.event.pageY+35)+"px")
            .style("width",100+"px")
            .style("height",30+"px")
            //                        .style("background","red")
            .style("display","block")
            .style("position","absolute")
            .style("opacity",1).html(
            "ActiveDegree:&nbsp&nbsp"+ d.ActiveDegree+"%"+"<br>TotalMsgCount:&nbsp&nbsp"+d.TotalMsgCount+
            "<br>UserCount:&nbsp&nbsp"+d.UserCount)

    })
    .on("mouseout",function () {
        tip_TotalMsgCount.style("display","none")
        tipText_TotalMsgCount.style("display","none")
    })
var label_TotalMsgCount = svg_TotalMsgCount.append("text")
    .attr("class", "label")
    .attr("text-anchor", "end")
    .attr("x", 500)
    .attr("y", 680)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_TotalMsgCount = label_TotalMsgCount.node().getBBox();
var yearScale_TotalMsgCount = d3.scale.linear()
    .domain([1, 7])
    .range([box_TotalMsgCount.x + 10, box_TotalMsgCount.x + box_TotalMsgCount.width - 10])
    .clamp(true);
//    var dataScale = d3.time.scale()
//            .domain([new Date(2015,0,2),new Date(2015,0,5)])
//            .range([box_TotalMsgCount.x + 10, box_TotalMsgCount.x + box_TotalMsgCount.width - 10])
//            .clamp(true);
var yearbox_TotalMsgCount = svg_TotalMsgCount.append("rect")
    .attr("class","yearbox")
    .attr("x", box_TotalMsgCount.x)
    .attr("y", box_TotalMsgCount.y)
    .attr("width", box_TotalMsgCount.width)
    .attr("height", box_TotalMsgCount.height)
    .on("mouseover", function () {
        console.log("进来了")
        yearbox_TotalMsgCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_TotalMsgCount.classed("active", true);
        }
        function mouseout() {
            label_TotalMsgCount.classed("active", false);
        }
        function mousemove() {
            changeDate(Math.round(yearScale_TotalMsgCount.invert(d3.mouse(this)[0])),"bubble_TotalMsgCount");
            label_TotalMsgCount.text(Math.round(yearScale_TotalMsgCount.invert(d3.mouse(this)[0])));
        }
    });
var tip_TotalMsgCount =svg_TotalMsgCount.append("rect")
    .attr("class","tip")
    .attr("width",130)
    .attr("height",50)
/*数据更新方法*/
var updateDate = function (newdata,className) {
    var update = d3.selectAll("."+className+" .dot")
        .data(newdata)


    switch (className) {
        case "bubble_TotalMsgCount" :
            var enter = update.enter();
            var exit = update.exit();
            update.transition()
                .duration(80)
                .attr("cx", function (d) {
                    return xScale_TotalMsgCount(d.ActiveDegree);
                })
                .attr("cy", function (d) {
                    return yScale_TotalMsgCount(d.TotalMsgCount);
                })
                .attr("r", function (d) {
                    return d.UserCount / 100;
                })

            exit.remove();
            break;
        case "bubble_ActiveUserCount":

            var enter = update.enter();
            var exit = update.exit();
            update.transition()
                .duration(80)
                .attr("cx", function (d) {
                    return xScale_ActiveUserCount(d.ActiveDegree);
                })
                .attr("cy", function (d) {
                    return yScale_ActiveUserCount(d.ActiveUserCount);
                })
                .attr("r", function (d) {
                    return d.UserCount / 100;
                })

            exit.remove();
            break;

        case "bubble_MonitorChatCount":

            var enter = update.enter();
            var exit = update.exit();
            update.transition()
                .duration(80)
                .attr("cx", function (d) {
                    return xScale_MonitorChatCount(d.ActiveDegree);
                })
                .attr("cy", function (d) {
                    return yScale_MonitorChatCount(d.MonitorChatCount);
                })
                .attr("r", function (d) {
                    return d.UserCount / 100;
                })

            exit.remove();
            break;

        case "bubble_MonitorMentioned":

            var enter = update.enter();
            var exit = update.exit();
            update.transition()
                .duration(80)
                .attr("cx", function (d) {
                    return xScale_MonitorMentioned(d.ActiveDegree);
                })
                .attr("cy", function (d) {
                    return yScale_MonitorMentioned(d.MonitorMentioned);
                })
                .attr("r", function (d) {
                    return d.UserCount / 100;
                })

            exit.remove();
            break;

        case "bubble_Keyword_Blocked":

            var enter = update.enter();
            var exit = update.exit();
            update.transition()
                .duration(80)
                .attr("cx", function (d) {
                    return xScale_Keyword_Blocked(d.ActiveDegree);
                })
                .attr("cy", function (d) {
                    return yScale_Keyword_Blocked(d.Blocked_JZ+d.Blocked_QF);
                })
                .attr("r", function (d) {
                    return d.UserCount / 100;
                })

            exit.remove();
            break;

        case "bubble_Keyword_WhiteList":

            var enter = update.enter();
            var exit = update.exit();
            update.transition()
                .duration(80)
                .attr("cx", function (d) {
                    return xScale_Keyword_WhiteList(d.ActiveDegree);
                })
                .attr("cy", function (d) {
                    return yScale_Keyword_WhiteList(d.BrandsRelated+d.FeedingRelated+d.JinZhuang+d.QiFu);
                })
                .attr("r", function (d) {
                    return d.UserCount / 100;
                })

            exit.remove();
            break;
    }
}


//    ActiveDegree
//    TotalMsgCount

/*活跃发言度图表*/

var bubble_ActiveUserCount= d3.selectAll(".bubble_ActiveUserCount")
var svg_ActiveUserCount = bubble_ActiveUserCount.append("svg")
    .attr("width",width)
    .attr("height",height)
var playground = svg_ActiveUserCount.append("rect")
    .attr("x", 40)
    .attr("y", 20)
    .attr("width", width-400)
    .attr("height", height-200)
    .style("fill","rgb(245,248,253)")

var color = d3.scale.category20c();                 //设置不同颜色
console.log("color"+color(1));

var xScale_ActiveUserCount = d3.scale.linear()
    .domain([0,100])
    .range([0,600])
var yScale_ActiveUserCount = d3.scale.linear()
    .domain([500,0])
    .range([0,500]);

var xAxis_ActiveUserCount = d3.svg.axis()
    .scale(xScale_ActiveUserCount)
    .orient("bottom")
// .ticks(6)
var yAxis_ActiveUserCount = d3.svg.axis()
    .scale(yScale_ActiveUserCount)
    .orient("left")

var xline_ActiveUserCount = d3.svg.axis()
    .scale(xScale_ActiveUserCount)
    .tickSize(500,0,0)
    .orient("bottom")
var yline_ActiveUserCount = d3.svg.axis()
    .scale(yScale_ActiveUserCount)
    .tickSize(900,0,0)
    .orient("left")
var xgrid = svg_ActiveUserCount.append("g")
    .attr("class","grid")
    .attr("transform","translate(40,20)")
    .call(xline_ActiveUserCount)
    .selectAll("text")
    .text("")
var ygrid = svg_ActiveUserCount.append("g")
    .attr("class","grid")
    .attr("transform","translate(640,20)")
    .call(yline_ActiveUserCount)
    .selectAll("text")
    .text("")
var gxAxis = svg_ActiveUserCount.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,520)")
    .call(xAxis_ActiveUserCount)
    .append("text")
    .text("ActiveDegree/%")
    .attr("transform","translate(620,2)")

var gyAxis = svg_ActiveUserCount.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,20)")
    .call(yAxis_ActiveUserCount)
    .append("text")
    .text("ActiveUserCount")
    .attr("transform","translate(10,-5)")
var tipText_ActiveUserCount = d3.selectAll(".bubble_ActiveUserCount .tip-text")
svg_ActiveUserCount.append("g")
    .attr("transform","translate(40,20)")
    .selectAll(".dot")
    .data(data[0])
    .enter()
    .append("circle")
    .attr("class","dot")
    .attr("cx", function(d) { return xScale_ActiveUserCount(d.ActiveDegree);})
    .attr("cy", function(d) { return yScale_ActiveUserCount(d.ActiveUserCount);})
    .attr("r", function (d) { return (d.UserCount/100);
    })
    .style("fill",function (d) {
        return color(1)
    })
    .attr("transform","translate(0,0)")
    .on("mouseover",function (d) {
        tip_ActiveUserCount.attr("fill","red")
            .attr("x",((d3.event.pageX))+"px")
            .attr("y",(d3.event.pageY+10)+"px")
            .style("display","block")
            .style("opacity",0.1)
            .style("background","red")
            .style("box-sizing","border-box")
            .style("border-radius",2+"px")

        tipText_ActiveUserCount
            .style("left",((d3.event.pageX)+10)+"px")
            .style("top",(d3.event.pageY+35)+"px")
            .style("width",100+"px")
            .style("height",30+"px")
            //                        .style("background","red")
            .style("display","block")
            .style("position","absolute")
            .style("opacity",1).html(
            "ActiveDegree:&nbsp&nbsp"+ d.ActiveDegree+"%"+"<br>ActiveUserCount:&nbsp&nbsp"+d.ActiveUserCount+
            "<br>UserCount:&nbsp&nbsp"+d.UserCount)

    })
    .on("mouseout",function () {
        tip_ActiveUserCount.style("display","none")
        tipText_ActiveUserCount.style("display","none")
    })
var label_ActiveUserCount = svg_ActiveUserCount.append("text")
    .attr("class", "label")
    .attr("text-anchor", "end")
    .attr("x", 500)
    .attr("y", 680)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_ActiveUserCount = label_ActiveUserCount.node().getBBox();
var yearScale_ActiveUserCount = d3.scale.linear()
    .domain([1, 7])
    .range([box_ActiveUserCount.x + 10, box_ActiveUserCount.x + box_ActiveUserCount.width - 10])
    .clamp(true);
var dataScale = d3.time.scale()
    .domain([new Date(2015,0,2),new Date(2015,0,5)])
    .range([box_ActiveUserCount.x + 10, box_ActiveUserCount.x + box_ActiveUserCount.width - 10])
    .clamp(true);
var yearbox_ActiveUserCount = svg_ActiveUserCount.append("rect")
    .attr("class","yearbox")
    .attr("x", box_ActiveUserCount.x)
    .attr("y", box_ActiveUserCount.y)
    .attr("width", box_ActiveUserCount.width)
    .attr("height", box_ActiveUserCount.height)
    .on("mouseover", function () {
        console.log("进来了")
        yearbox_ActiveUserCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_ActiveUserCount.classed("active", true);
        }
        function mouseout() {
            label_ActiveUserCount.classed("active", false);
        }
        function mousemove() {
            changeDate(Math.round(yearScale_ActiveUserCount.invert(d3.mouse(this)[0])),"bubble_ActiveUserCount");
            label_ActiveUserCount.text(Math.round(yearScale_ActiveUserCount.invert(d3.mouse(this)[0])));
        }
    });
var tip_ActiveUserCount =svg_ActiveUserCount.append("rect")
    .attr("class","tip")
    .attr("width",130)
    .attr("height",50)


/*MonitorChatCount图表*/

var bubble_MonitorChatCount= d3.selectAll(".bubble_MonitorChatCount")
var svg_MonitorChatCount = bubble_MonitorChatCount.append("svg")
    .attr("width",width)
    .attr("height",height)
var playground = svg_MonitorChatCount.append("rect")
    .attr("x", 40)
    .attr("y", 20)
    .attr("width", width-400)
    .attr("height", height-200)
    .style("fill","rgb(245,248,253)")

var color = d3.scale.category20c();                 //设置不同颜色
console.log("color"+color(1));

var xScale_MonitorChatCount = d3.scale.linear()
    .domain([0,100])
    .range([0,600])
var yScale_MonitorChatCount = d3.scale.linear()
    .domain([100,0])
    .range([0,500]);

var xAxis_MonitorChatCount = d3.svg.axis()
    .scale(xScale_MonitorChatCount)
    .orient("bottom")
// .ticks(6)
var yAxis_MonitorChatCount = d3.svg.axis()
    .scale(yScale_MonitorChatCount)
    .orient("left")

var xline_MonitorChatCount = d3.svg.axis()
    .scale(xScale_MonitorChatCount)
    .tickSize(500,0,0)
    .orient("bottom")
var yline_MonitorChatCount = d3.svg.axis()
    .scale(yScale_MonitorChatCount)
    .tickSize(900,0,0)
    .orient("left")
var xgrid = svg_MonitorChatCount.append("g")
    .attr("class","grid")
    .attr("transform","translate(40,20)")
    .call(xline_MonitorChatCount)
    .selectAll("text")
    .text("")
var ygrid = svg_MonitorChatCount.append("g")
    .attr("class","grid")
    .attr("transform","translate(640,20)")
    .call(yline_MonitorChatCount)
    .selectAll("text")
    .text("")
var gxAxis = svg_MonitorChatCount.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,520)")
    .call(xAxis_MonitorChatCount)
    .append("text")
    .text("ActiveDegree/%")
    .attr("transform","translate(620,2)")

var gyAxis = svg_MonitorChatCount.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,20)")
    .call(yAxis_MonitorChatCount)
    .append("text")
    .text("MonitorChatCount")
    .attr("transform","translate(10,-5)")
var tipText_MonitorChatCount = d3.selectAll(".bubble_MonitorChatCount .tip-text")
svg_MonitorChatCount.append("g")
    .attr("transform","translate(40,20)")
    .selectAll(".dot")
    .data(data[0])
    .enter()
    .append("circle")
    .attr("class","dot")
    .attr("cx", function(d) { return xScale_MonitorChatCount(d.ActiveDegree);})
    .attr("cy", function(d) { return yScale_MonitorChatCount(d.MonitorChatCount);})
    .attr("r", function (d) { return (d.UserCount/100);
    })
    .style("fill",function (d) {
        return color(1)
    })
    .attr("transform","translate(0,0)")
    .on("mouseover",function (d) {
        tip_MonitorChatCount.attr("fill","red")
            .attr("x",((d3.event.pageX))+"px")
            .attr("y",(d3.event.pageY+10)+"px")
            .style("display","block")
            .style("opacity",0.1)
            .style("background","red")
            .style("box-sizing","border-box")
            .style("border-radius",2+"px")

        tipText_MonitorChatCount
            .style("left",((d3.event.pageX)+10)+"px")
            .style("top",(d3.event.pageY+35)+"px")
            .style("width",100+"px")
            .style("height",30+"px")
            //                        .style("background","red")
            .style("display","block")
            .style("position","absolute")
            .style("opacity",1).html(
            "ActiveDegree:&nbsp&nbsp"+ d.ActiveDegree+"%"+"<br>MonitorChatCount:&nbsp&nbsp"+d.MonitorChatCount+
            "<br>UserCount:&nbsp&nbsp"+d.UserCount)

    })
    .on("mouseout",function () {
        tip_MonitorChatCount.style("display","none")
        tipText_MonitorChatCount.style("display","none")
    })
var label_MonitorChatCount = svg_MonitorChatCount.append("text")
    .attr("class", "label")
    .attr("text-anchor", "end")
    .attr("x", 500)
    .attr("y", 680)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_MonitorChatCount = label_MonitorChatCount.node().getBBox();
var yearScale_MonitorChatCount = d3.scale.linear()
    .domain([1, 7])
    .range([box_MonitorChatCount.x + 10, box_MonitorChatCount.x + box_MonitorChatCount.width - 10])
    .clamp(true);
var dataScale = d3.time.scale()
    .domain([new Date(2015,0,2),new Date(2015,0,5)])
    .range([box_MonitorChatCount.x + 10, box_MonitorChatCount.x + box_MonitorChatCount.width - 10])
    .clamp(true);
var yearbox_MonitorChatCount = svg_MonitorChatCount.append("rect")
    .attr("class","yearbox")
    .attr("x", box_MonitorChatCount.x)
    .attr("y", box_MonitorChatCount.y)
    .attr("width", box_MonitorChatCount.width)
    .attr("height", box_MonitorChatCount.height)
    .on("mouseover", function () {
        console.log("进来了")
        yearbox_MonitorChatCount
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_MonitorChatCount.classed("active", true);
        }
        function mouseout() {
            label_MonitorChatCount.classed("active", false);
        }
        function mousemove() {
            changeDate(Math.round(yearScale_MonitorChatCount.invert(d3.mouse(this)[0])),"bubble_MonitorChatCount");
            label_MonitorChatCount.text(Math.round(yearScale_MonitorChatCount.invert(d3.mouse(this)[0])));
        }
    });
var tip_MonitorChatCount =svg_MonitorChatCount.append("rect")
    .attr("class","tip")
    .attr("width",130)
    .attr("height",50)


/*MonitorMentioned图表*/

var bubble_MonitorMentioned= d3.selectAll(".bubble_MonitorMentioned")
var svg_MonitorMentioned = bubble_MonitorMentioned.append("svg")
    .attr("width",width)
    .attr("height",height)
var playground = svg_MonitorMentioned.append("rect")
    .attr("x", 40)
    .attr("y", 20)
    .attr("width", width-400)
    .attr("height", height-200)
    .style("fill","rgb(245,248,253)")

var color = d3.scale.category20c();                 //设置不同颜色
console.log("color"+color(1));

var xScale_MonitorMentioned = d3.scale.linear()
    .domain([0,100])
    .range([0,600])
var yScale_MonitorMentioned = d3.scale.linear()
    .domain([100,0])
    .range([0,500]);

var xAxis_MonitorMentioned = d3.svg.axis()
    .scale(xScale_MonitorMentioned)
    .orient("bottom")
// .ticks(6)
var yAxis_MonitorMentioned = d3.svg.axis()
    .scale(yScale_MonitorMentioned)
    .orient("left")

var xline_MonitorMentioned = d3.svg.axis()
    .scale(xScale_MonitorMentioned)
    .tickSize(500,0,0)
    .orient("bottom")
var yline_MonitorMentioned = d3.svg.axis()
    .scale(yScale_MonitorMentioned)
    .tickSize(900,0,0)
    .orient("left")
var xgrid = svg_MonitorMentioned.append("g")
    .attr("class","grid")
    .attr("transform","translate(40,20)")
    .call(xline_MonitorMentioned)
    .selectAll("text")
    .text("")
var ygrid = svg_MonitorMentioned.append("g")
    .attr("class","grid")
    .attr("transform","translate(640,20)")
    .call(yline_MonitorMentioned)
    .selectAll("text")
    .text("")
var gxAxis = svg_MonitorMentioned.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,520)")
    .call(xAxis_MonitorMentioned)
    .append("text")
    .text("ActiveDegree/%")
    .attr("transform","translate(620,2)")

var gyAxis = svg_MonitorMentioned.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,20)")
    .call(yAxis_MonitorMentioned)
    .append("text")
    .text("MonitorMentioned")
    .attr("transform","translate(10,-5)")
var tipText_MonitorMentioned = d3.selectAll(".bubble_MonitorMentioned .tip-text")
svg_MonitorMentioned.append("g")
    .attr("transform","translate(40,20)")
    .selectAll(".dot")
    .data(data[0])
    .enter()
    .append("circle")
    .attr("class","dot")
    .attr("cx", function(d) { return xScale_MonitorMentioned(d.ActiveDegree);})
    .attr("cy", function(d) { return yScale_MonitorMentioned(d.MonitorMentioned);})
    .attr("r", function (d) { return (d.UserCount/100);
    })
    .style("fill",function (d) {
        return color(1)
    })
    .attr("transform","translate(0,0)")
    .on("mouseover",function (d) {
        tip_MonitorMentioned.attr("fill","red")
            .attr("x",((d3.event.pageX))+"px")
            .attr("y",(d3.event.pageY+10)+"px")
            .style("display","block")
            .style("opacity",0.1)
            .style("background","red")
            .style("box-sizing","border-box")
            .style("border-radius",2+"px")

        tipText_MonitorMentioned
            .style("left",((d3.event.pageX)+10)+"px")
            .style("top",(d3.event.pageY+35)+"px")
            .style("width",100+"px")
            .style("height",30+"px")
            //                        .style("background","red")
            .style("display","block")
            .style("position","absolute")
            .style("opacity",1).html(
            "ActiveDegree:&nbsp&nbsp"+ d.ActiveDegree+"%"+"<br>MonitorMentioned:&nbsp&nbsp"+d.MonitorMentioned+
            "<br>UserCount:&nbsp&nbsp"+d.UserCount)

    })
    .on("mouseout",function () {
        tip_MonitorMentioned.style("display","none")
        tipText_MonitorMentioned.style("display","none")
    })
var label_MonitorMentioned = svg_MonitorMentioned.append("text")
    .attr("class", "label")
    .attr("text-anchor", "end")
    .attr("x", 500)
    .attr("y", 680)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_MonitorMentioned = label_MonitorMentioned.node().getBBox();
var yearScale_MonitorMentioned = d3.scale.linear()
    .domain([1, 7])
    .range([box_MonitorMentioned.x + 10, box_MonitorMentioned.x + box_MonitorMentioned.width - 10])
    .clamp(true);
var dataScale = d3.time.scale()
    .domain([new Date(2015,0,2),new Date(2015,0,5)])
    .range([box_MonitorMentioned.x + 10, box_MonitorMentioned.x + box_MonitorMentioned.width - 10])
    .clamp(true);
var yearbox_MonitorMentioned = svg_MonitorMentioned.append("rect")
    .attr("class","yearbox")
    .attr("x", box_MonitorMentioned.x)
    .attr("y", box_MonitorMentioned.y)
    .attr("width", box_MonitorMentioned.width)
    .attr("height", box_MonitorMentioned.height)
    .on("mouseover", function () {
        console.log("进来了")
        yearbox_MonitorMentioned
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_MonitorMentioned.classed("active", true);
        }
        function mouseout() {
            label_MonitorMentioned.classed("active", false);
        }
        function mousemove() {
            changeDate(Math.round(yearScale_MonitorMentioned.invert(d3.mouse(this)[0])),"bubble_MonitorMentioned");
            label_MonitorMentioned.text(Math.round(yearScale_MonitorMentioned.invert(d3.mouse(this)[0])));
        }
    });
var tip_MonitorMentioned =svg_MonitorMentioned.append("rect")
    .attr("class","tip")
    .attr("width",130)
    .attr("height",50)


/*Keyword_WhiteList图表*/

var bubble_Keyword_WhiteList= d3.selectAll(".bubble_Keyword_WhiteList")
var svg_Keyword_WhiteList = bubble_Keyword_WhiteList.append("svg")
    .attr("width",width)
    .attr("height",height)
var playground = svg_Keyword_WhiteList.append("rect")
    .attr("x", 40)
    .attr("y", 20)
    .attr("width", width-400)
    .attr("height", height-200)
    .style("fill","rgb(245,248,253)")

var color = d3.scale.category20c();                 //设置不同颜色
console.log("color"+color(1));

var xScale_Keyword_WhiteList = d3.scale.linear()
    .domain([0,100])
    .range([0,600])
var yScale_Keyword_WhiteList = d3.scale.linear()
    .domain([50,0])
    .range([0,500]);

var xAxis_Keyword_WhiteList = d3.svg.axis()
    .scale(xScale_Keyword_WhiteList)
    .orient("bottom")
// .ticks(6)
var yAxis_Keyword_WhiteList = d3.svg.axis()
    .scale(yScale_Keyword_WhiteList)
    .orient("left")

var xline_Keyword_WhiteList = d3.svg.axis()
    .scale(xScale_Keyword_WhiteList)
    .tickSize(500,0,0)
    .orient("bottom")
var yline_Keyword_WhiteList = d3.svg.axis()
    .scale(yScale_Keyword_WhiteList)
    .tickSize(900,0,0)
    .orient("left")
var xgrid = svg_Keyword_WhiteList.append("g")
    .attr("class","grid")
    .attr("transform","translate(40,20)")
    .call(xline_Keyword_WhiteList)
    .selectAll("text")
    .text("")
var ygrid = svg_Keyword_WhiteList.append("g")
    .attr("class","grid")
    .attr("transform","translate(640,20)")
    .call(yline_Keyword_WhiteList)
    .selectAll("text")
    .text("")
var gxAxis = svg_Keyword_WhiteList.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,520)")
    .call(xAxis_Keyword_WhiteList)
    .append("text")
    .text("ActiveDegree/%")
    .attr("transform","translate(620,2)")

var gyAxis = svg_Keyword_WhiteList.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,20)")
    .call(yAxis_Keyword_WhiteList)
    .append("text")
    .text("Keyword_WhiteList")
    .attr("transform","translate(10,-5)")
var tipText_Keyword_WhiteList = d3.selectAll(".bubble_Keyword_WhiteList .tip-text")
svg_Keyword_WhiteList.append("g")
    .attr("transform","translate(40,20)")
    .selectAll(".dot")
    .data(data[0])
    .enter()
    .append("circle")
    .attr("class","dot")
    .attr("cx", function(d) { return xScale_Keyword_WhiteList(d.ActiveDegree);})
    .attr("cy", function(d) { return yScale_Keyword_WhiteList(d.BrandsRelated+d.FeedingRelated+d.JinZhuang+d.QiFu);})
    .attr("r", function (d) { return (d.UserCount/100);
    })
    .style("fill",function (d) {
        return color(1)
    })
    .attr("transform","translate(0,0)")
    .on("mouseover",function (d) {
        tip_Keyword_WhiteList.attr("fill","red")
            .attr("x",((d3.event.pageX))+"px")
            .attr("y",(d3.event.pageY+10)+"px")
            .style("display","block")
            .style("opacity",0.1)
            .style("background","red")
            .style("box-sizing","border-box")
            .style("border-radius",2+"px")

        tipText_Keyword_WhiteList
            .style("left",((d3.event.pageX)+10)+"px")
            .style("top",(d3.event.pageY+35)+"px")
            .style("width",100+"px")
            .style("height",30+"px")
            //                        .style("background","red")
            .style("display","block")
            .style("position","absolute")
            .style("opacity",1).html(
            "ActiveDegree:&nbsp&nbsp"+ d.ActiveDegree+"%"+"<br>WhiteList_KeywordCount:&nbsp&nbsp" +(d.BrandsRelated+d.FeedingRelated+d.JinZhuang+d.QiFu)+
            "<br>UserCount:&nbsp&nbsp"+d.UserCount)

    })
    .on("mouseout",function () {
        tip_Keyword_WhiteList.style("display","none")
        tipText_Keyword_WhiteList.style("display","none")
    })
var label_Keyword_WhiteList = svg_Keyword_WhiteList.append("text")
    .attr("class", "label")
    .attr("text-anchor", "end")
    .attr("x", 500)
    .attr("y", 680)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_Keyword_WhiteList = label_Keyword_WhiteList.node().getBBox();
var yearScale_Keyword_WhiteList = d3.scale.linear()
    .domain([1, 7])
    .range([box_Keyword_WhiteList.x + 10, box_Keyword_WhiteList.x + box_Keyword_WhiteList.width - 10])
    .clamp(true);
var dataScale = d3.time.scale()
    .domain([new Date(2015,0,2),new Date(2015,0,5)])
    .range([box_Keyword_WhiteList.x + 10, box_Keyword_WhiteList.x + box_Keyword_WhiteList.width - 10])
    .clamp(true);
var yearbox_Keyword_WhiteList = svg_Keyword_WhiteList.append("rect")
    .attr("class","yearbox")
    .attr("x", box_Keyword_WhiteList.x)
    .attr("y", box_Keyword_WhiteList.y)
    .attr("width", box_Keyword_WhiteList.width)
    .attr("height", box_Keyword_WhiteList.height)
    .on("mouseover", function () {
        console.log("进来了")
        yearbox_Keyword_WhiteList
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_Keyword_WhiteList.classed("active", true);
        }
        function mouseout() {
            label_Keyword_WhiteList.classed("active", false);
        }
        function mousemove() {
            changeDate(Math.round(yearScale_Keyword_WhiteList.invert(d3.mouse(this)[0])),"bubble_Keyword_WhiteList");
            label_Keyword_WhiteList.text(Math.round(yearScale_Keyword_WhiteList.invert(d3.mouse(this)[0])));
        }
    });
var tip_Keyword_WhiteList =svg_Keyword_WhiteList.append("rect")
    .attr("class","tip")
    .attr("width",130)
    .attr("height",50)


/*Keyword_Blocked图表*/

var bubble_Keyword_Blocked= d3.selectAll(".bubble_Keyword_Blocked")
var svg_Keyword_Blocked = bubble_Keyword_Blocked.append("svg")
    .attr("width",width)
    .attr("height",height)
var playground = svg_Keyword_Blocked.append("rect")
    .attr("x", 40)
    .attr("y", 20)
    .attr("width", width-400)
    .attr("height", height-200)
    .style("fill","rgb(245,248,253)")

var color = d3.scale.category20c();                 //设置不同颜色
console.log("color"+color(1));

var xScale_Keyword_Blocked = d3.scale.linear()
    .domain([0,100])
    .range([0,600])
var yScale_Keyword_Blocked = d3.scale.linear()
    .domain([10,0])
    .range([0,500]);

var xAxis_Keyword_Blocked = d3.svg.axis()
    .scale(xScale_Keyword_Blocked)
    .orient("bottom")
// .ticks(6)
var yAxis_Keyword_Blocked = d3.svg.axis()
    .scale(yScale_Keyword_Blocked)
    .orient("left")

var xline_Keyword_Blocked = d3.svg.axis()
    .scale(xScale_Keyword_Blocked)
    .tickSize(500,0,0)
    .orient("bottom")
var yline_Keyword_Blocked = d3.svg.axis()
    .scale(yScale_Keyword_Blocked)
    .tickSize(900,0,0)
    .orient("left")
var xgrid = svg_Keyword_Blocked.append("g")
    .attr("class","grid")
    .attr("transform","translate(40,20)")
    .call(xline_Keyword_Blocked)
    .selectAll("text")
    .text("")
var ygrid = svg_Keyword_Blocked.append("g")
    .attr("class","grid")
    .attr("transform","translate(640,20)")
    .call(yline_Keyword_Blocked)
    .selectAll("text")
    .text("")
var gxAxis = svg_Keyword_Blocked.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,520)")
    .call(xAxis_Keyword_Blocked)
    .append("text")
    .text("ActiveDegree/%")
    .attr("transform","translate(620,2)")

var gyAxis = svg_Keyword_Blocked.append("g")
    .attr("class","axis")
    .attr("transform","translate(40,20)")
    .call(yAxis_Keyword_Blocked)
    .append("text")
    .text("Keyword_Blocked")
    .attr("transform","translate(10,-5)")
var tipText_Keyword_Blocked = d3.selectAll(".bubble_Keyword_Blocked .tip-text")
svg_Keyword_Blocked.append("g")
    .attr("transform","translate(40,20)")
    .selectAll(".dot")
    .data(data[0])
    .enter()
    .append("circle")
    .attr("class","dot")
    .attr("cx", function(d) { return xScale_Keyword_Blocked(d.ActiveDegree);})
    .attr("cy", function(d) { return yScale_Keyword_Blocked(d.Blocked_JZ+d.Blocked_QF);})
    .attr("r", function (d) { return (d.UserCount/100);
    })
    .style("fill",function (d) {
        return color(1)
    })
    .attr("transform","translate(0,0)")
    .on("mouseover",function (d) {
        tip_Keyword_Blocked.attr("fill","red")
            .attr("x",((d3.event.pageX))+"px")
            .attr("y",(d3.event.pageY+10)+"px")
            .style("display","block")
            .style("opacity",0.1)
            .style("background","red")
            .style("box-sizing","border-box")
            .style("border-radius",2+"px")

        tipText_Keyword_Blocked
            .style("left",((d3.event.pageX)+10)+"px")
            .style("top",(d3.event.pageY+35)+"px")
            .style("width",100+"px")
            .style("height",30+"px")
            //                        .style("background","red")
            .style("display","block")
            .style("position","absolute")
            .style("opacity",1).html(
            "ActiveDegree:&nbsp&nbsp"+ d.ActiveDegree+"%"+"<br>Blocked_KeywordCount:&nbsp&nbsp" +(d.Blocked_JZ+d.Blocked_QF)+
            "<br>UserCount:&nbsp&nbsp"+d.UserCount)

    })
    .on("mouseout",function () {
        tip_Keyword_Blocked.style("display","none")
        tipText_Keyword_Blocked.style("display","none")
    })
var label_Keyword_Blocked = svg_Keyword_Blocked.append("text")
    .attr("class", "label")
    .attr("text-anchor", "end")
    .attr("x", 500)
    .attr("y", 680)
    .attr("width",width-100)
    .attr("height",30)
    .text(1);
var box_Keyword_Blocked = label_Keyword_Blocked.node().getBBox();
var yearScale_Keyword_Blocked = d3.scale.linear()
    .domain([1, 7])
    .range([box_Keyword_Blocked.x + 10, box_Keyword_Blocked.x + box_Keyword_Blocked.width - 10])
    .clamp(true);
var dataScale = d3.time.scale()
    .domain([new Date(2015,0,2),new Date(2015,0,5)])
    .range([box_Keyword_Blocked.x + 10, box_Keyword_Blocked.x + box_Keyword_Blocked.width - 10])
    .clamp(true);
var yearbox_Keyword_Blocked = svg_Keyword_Blocked.append("rect")
    .attr("class","yearbox")
    .attr("x", box_Keyword_Blocked.x)
    .attr("y", box_Keyword_Blocked.y)
    .attr("width", box_Keyword_Blocked.width)
    .attr("height", box_Keyword_Blocked.height)
    .on("mouseover", function () {
        console.log("进来了")
        yearbox_Keyword_Blocked
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove)
            .on("touchmove", mousemove);
        function mouseover() {
            label_Keyword_Blocked.classed("active", true);
        }
        function mouseout() {
            label_Keyword_Blocked.classed("active", false);
        }
        function mousemove() {
            changeDate(Math.round(yearScale_Keyword_Blocked.invert(d3.mouse(this)[0])),"bubble_Keyword_Blocked");
            label_Keyword_Blocked.text(Math.round(yearScale_Keyword_Blocked.invert(d3.mouse(this)[0])));
        }
    });
var tip_Keyword_Blocked =svg_Keyword_Blocked.append("rect")
    .attr("class","tip")
    .attr("width",130)
    .attr("height",50)